import { NestFactory } from "@nestjs/core";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";
import { FastifyAdapter, NestFastifyApplication } from "@nestjs/platform-fastify";

import { AppModule } from "app.module";

async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(AppModule, new FastifyAdapter());
  app.setGlobalPrefix("api");

  const options = new DocumentBuilder().setTitle("Template").build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup("api/swagger", app, document);

  await app.listen(8180);
}

bootstrap();
