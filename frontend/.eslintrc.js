module.exports = {
  parser: "@typescript-eslint/parser",
  parserOptions: {
    project: "tsconfig.json",
    tsconfigRootDir: __dirname,
    sourceType: "module",
    ecmaFeatures: {
      jsx: true,
    },
  },
  root: true,
  env: {
    browser: true,
    es2022: true,
  },
  plugins: ["react"],
  extends: ["plugin:react/recommended", "plugin:react/jsx-runtime", "../.eslintrc.js"],
  rules: {
    "react/prop-types": "off",
  },
  ignorePatterns: ["vite.config.ts", "jest-setup.ts", "babel.config.js"],
};
