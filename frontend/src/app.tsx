import { Hello } from "components/hello";

export const App = () => (
  <div>
    <h1>Look ma, absolute import!</h1>
    <Hello />
  </div>
);
