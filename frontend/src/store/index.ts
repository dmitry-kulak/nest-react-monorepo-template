import { configureStore } from "@reduxjs/toolkit";

import { exampleReducer } from "./slices/example-slice";
import { exampleApiMiddleware, exampleApiReducer, exampleApiReducerPath } from "./example-api";

export const store = configureStore({
  reducer: { example: exampleReducer, [exampleApiReducerPath]: exampleApiReducer },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(exampleApiMiddleware),
});
