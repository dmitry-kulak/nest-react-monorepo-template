import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const exampleApiSlice = createApi({
  reducerPath: "api",
  baseQuery: fetchBaseQuery({
    baseUrl: "/api",
  }),
  endpoints: (builder) => ({
    root: builder.query<{ message: string }, void>({
      query: () => "",
    }),
  }),
});

export const exampleApiReducer = exampleApiSlice.reducer;
export const exampleApiReducerPath = exampleApiSlice.reducerPath;
export const exampleApiMiddleware = exampleApiSlice.middleware;

export const { useRootQuery } = exampleApiSlice;
