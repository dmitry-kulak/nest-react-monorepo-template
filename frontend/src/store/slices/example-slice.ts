import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export const exampleSlice = createSlice({
  name: "example",
  initialState: "Type something!",
  reducers: {
    setText: (state, { payload }: PayloadAction<string>) => (state = payload),
  },
});

export const { setText } = exampleSlice.actions;
export const exampleReducer = exampleSlice.reducer;
