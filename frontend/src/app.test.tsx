import { render, screen } from "@testing-library/react";
import { Provider } from "react-redux";

import { App } from "app";
import { store } from "store";

describe("App", () => {
  it("should render", () => {
    render(
      <Provider store={store}>
        <App />
      </Provider>
    );

    expect(screen.getByText("Look ma, absolute import!")).toBeInTheDocument();
  });
});
