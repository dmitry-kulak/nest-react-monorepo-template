import { SyntheticEvent } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { useAppDispatch, useAppSelector } from "store/hooks";

import { useRootQuery } from "store/example-api";
import { setText } from "store/slices/example-slice";

type FormValues = { text: string };

export const Hello = () => {
  const text = useAppSelector((state) => state.example);
  const dispatch = useAppDispatch();

  const { data = { message: "Backend has not responded yet" }, refetch } = useRootQuery();

  const handleHiClick = (event: SyntheticEvent) => {
    event.preventDefault();
    refetch();
  };

  const { register, handleSubmit } = useForm<FormValues>();

  const onSubmit: SubmitHandler<FormValues> = (formData) => dispatch(setText(formData.text));

  return (
    <div>
      {data?.message || "Backend not responding"}
      <button onClick={handleHiClick}>Send hi to backend</button>

      <form onSubmit={handleSubmit(onSubmit)}>
        {text}
        <div>
          <input defaultValue={text} placeholder="Text" {...register("text", { required: true })} />
          <button>Save</button>
        </div>
      </form>
    </div>
  );
};
