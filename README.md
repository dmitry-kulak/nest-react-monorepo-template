# Nest Fullstack Template

## Requirements
* node >= 14 (16)
* yarn >= 1.22.10

## Features

### Backend
* nest 9
* typeorm
* swagger

### Frontend
* react 18
* react-toolkit
* react-hook-form
* vite-plugin-svgr for svg loading
* sass
* vite 

### Common
* jest
* yarn 3 with typescript and interactive plugin
* yarn workspaces
* eslint + prettier
* absolute imports

## Setup
From root
```
yarn
```

## Todo
* make tests work with svg
* launch everything from root
* serve static
* more packages
* docker
* husky
